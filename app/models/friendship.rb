class Friendship < ActiveRecord::Base

  enum status: [ :accepted, :pending ]
  scope :accepted, -> {where status: "0"}
  scope :pending, -> {where status: "1"}

  belongs_to :user
  belongs_to :friend, :class_name => 'User'


end
