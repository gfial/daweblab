class Team < ActiveRecord::Base

  has_and_belongs_to_many :users
  belongs_to :captain, class_name: 'User'
  validates_presence_of :captain

end
