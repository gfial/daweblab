class Place < ActiveRecord::Base

  has_many :activities
  validates_uniqueness_of :name


end
