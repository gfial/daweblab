class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :activities
  has_and_belongs_to_many :teams

  has_many :friendships
  has_many :friends, :through => :friendships

  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user

  def self.search(search, current_user)
    search_condition = "%" + search + "%"
    User.where("name LIKE (?)","%#{search_condition}%").where("id != #{current_user.id}")
    #find(:all, :conditions => [search_condition, search_condition])
  end

end
