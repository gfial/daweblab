class Activity < ActiveRecord::Base

  enum actype: [ :Swimming, :Running, :Cycling, :Gymn ]

  belongs_to :user
  belongs_to :place

  validates_presence_of :place
end
