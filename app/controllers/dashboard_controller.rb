class DashboardController < ApplicationController


  def index
    @teams = current_user.teams.where(captain_id: current_user.id)
    @activities = []
  end

end
