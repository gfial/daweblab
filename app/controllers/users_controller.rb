class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def show
    respond_to do |format|
      format.html
      format.xml {render :xml => @user}
    end
  end

  def search
    if params[:search].nil?
      @users = nil
    else
      @users = User.search(params[:search], current_user)
    end
  end

  def edit
  end

  def update
    userParams = user_params
    name = userParams['name'].split(' ').map {|f| f.capitalize}
    @user.name = name.join(' ')
    @user.age = userParams['age'].to_i
    @user.sex = userParams['sex']
    @user.save!
    redirect_to @user
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :age, :sex)
  end

end
