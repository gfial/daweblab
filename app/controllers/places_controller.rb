class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  def index
    @places = Place.all

  end

  def show
    @users = User.find_by_sql("SELECT users_at_place.*
      FROM
      (
      SELECT users.*
      FROM users INNER JOIN activities ON (users.id = activities.user_id)
      WHERE activities.place_id = #{@place.id} AND
      (datetime(activities.date, '+' || activities.duration || ' seconds') > datetime('now','utc') OR
       activities.duration IS NULL AND datetime(activities.date) < datetime('now','utc')) AND
      users.id <> #{current_user.id}
      ) AS users_at_place INNER JOIN friendships ON ((users_at_place.id = friendships.friend_id) OR (users_at_place.id = friendships.user_id))
      WHERE friendships.user_id = #{current_user.id} OR friendships.friend_id = #{current_user.id}")
  end

  def new
    @place = Place.new
  end

  def edit
  end

  def create
    @place = Place.new(place_params)
    @place.save!
  end

  def update
    @place.update(place_params)
    redirect_to @place
  end

  def destroy
    @place.destroy
  end

  private
    def set_place
      @place = Place.find(params[:id])
    end

    def place_params
      params.require(:place).permit(:name)
    end
end
