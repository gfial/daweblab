class FriendshipsController < ApplicationController


  def index
    if params[:filter].blank?
      @friends =  current_user.inverse_friends
    else
      @friends =  current_user.inverse_friends.where("name LIKE ?","%#{params[:filter]}%")
    end
  end

  def new
    @friendship = Friendship.new
  end

  def create
    @friendship = Friendship.new
    @friendship.friend_id = params[:friendship][:friend_id].to_i
    @friendship.status = "pending"
    if @friendship.save!
      current_user.friendships << @friendship
      current_user.save!
      flash[:notice] = "Request sent."
      redirect_to root_url
    else
      flash[:notice] = "Unable to add friend."
      redirect_to root_url
    end
  end

  def edit
    @friendship = Friendship.find(params[:id])
  end

  def update
    @friendship = Friendship.find(params[:id])
    @friendship.status = "accepted"
    if @friendship.save!
      flash[:notice] = "Added friend."
      redirect_to current_user
    end
  end

  def destroy
    @friendship = Friendship.find(params[:id])
    @friendship.destroy
    flash[:notice] = "Removed friendship."
    flash[:tab] = "Friends"
    redirect_to current_user
  end

end
