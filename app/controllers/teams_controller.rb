class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy, :addmember, :add]

  def index
    @teams = Team.all
  end

  def show

  end

  def new
    @team = Team.new
  end

  def edit
  end

  def create
    @team = Team.new(team_params)
    @team.captain = current_user
    @team.save!
    current_user.teams << @team
    current_user.save!
    redirect_to @team
  end

  def update
    @team.update(team_params)
    @team.save!
    redirect_to @team
  end

  def destroy
    @team.destroy
    redirect_to root_path
  end

  def add

  end

  def addmember
    @member = User.find(params[:team][:user_id])
    @team.users << @member
    @team.save!
    redirect_to @team
  end

  private
    def set_team
      @team = Team.find(params[:id])
    end

    def team_params
      params.require(:team).permit(:name)
    end
end
