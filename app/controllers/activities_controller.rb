class ActivitiesController < ApplicationController
  before_action :set_activity, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:index]
  @@j = ActiveSupport::JSON
  @@descending_sort = ->(a,b) {b.second <=> a.second}

  def index
    @activities = @user.activities.order(date: :desc)
  end

  def show
    @place = @activity.place
    @acattributes = @@j.decode(@activity.acattributes)
  end

  def new
    @activity = Activity.new
  end

  def edit

  end

  def create
    @activity = Activity.new(activity_params)
    attri = ""
    if @activity.Swimming? ||  @activity.Running? || @activity.Cycling? || @activity.Gymn?
      attri = @@j.encode(acattributes)
    end
    @activity.user_id = current_user.id
    placename = params[:activity][:place]
    @place = Place.find_by_name(placename)
    if @place.nil?
      @place = Place.new
      @place.name = placename
      @place.save!
    end
    @activity.place_id = @place.id
    @activity.acattributes = attri
    @activity.save!
    @place.activities << @activity
    current_user.activities << @activity
    @place.save!
    current_user.save!
    redirect_to @activity
  end

  def update
    @activity.update(activity_params)
    attri = ""
    #if @activity.Swimming? ||  @activity.Running? || @activity.Cycling? || @activity.Gymn?
      attri = @@j.encode(acattributes)
    #end
    @activity.acattributes = attri
    @activity.save!
    render 'activities/update'
    redirect_to @activity
  end

  def destroy
    @activity.destroy
    #para tds as actividds
    redirect_to current_user
  end

  def leaderboards
    lglobal = Activity.joins('JOIN friendships').where("friendships.status = 0 AND ((activities.user_id = friendships.user_id AND friendships.friend_id = #{current_user.id}) OR (activities.user_id = friendships.friend_id AND friendships.user_id = #{current_user.id}) OR activities.user_id = #{current_user.id})").uniq
    lswim = Activity.where(actype: 0).joins('JOIN friendships').where("friendships.status = 0 AND ((activities.user_id = friendships.user_id AND friendships.friend_id = #{current_user.id}) OR (activities.user_id = friendships.friend_id AND friendships.user_id = #{current_user.id}) OR activities.user_id = #{current_user.id})").uniq
    lrun = Activity.where(actype: 1).joins('JOIN friendships').where("friendships.status = 0 AND ((activities.user_id = friendships.user_id AND friendships.friend_id = #{current_user.id}) OR (activities.user_id = friendships.friend_id AND friendships.user_id = #{current_user.id}) OR activities.user_id = #{current_user.id})").uniq
    lcycle = Activity.where(actype: 2).joins('JOIN friendships').where("friendships.status = 0 AND ((activities.user_id = friendships.user_id AND friendships.friend_id = #{current_user.id}) OR (activities.user_id = friendships.friend_id AND friendships.user_id = #{current_user.id}) OR activities.user_id = #{current_user.id})").uniq
    lgym = Activity.where(actype: 3).joins('JOIN friendships').where("friendships.status = 0 AND ((activities.user_id = friendships.user_id AND friendships.friend_id = #{current_user.id}) OR (activities.user_id = friendships.friend_id AND friendships.user_id = #{current_user.id}) OR activities.user_id = #{current_user.id})").uniq

    lglobalByFreq = lglobal.group('activities.user_id').count
    @lglobalByFreq = []
    lglobalByFreq.each { |a| @lglobalByFreq << a }
    @lglobalByFreq.sort!(& @@descending_sort)

    lswimByFreq = lswim.group('activities.user_id').count
    @lswimByFreq = []
    lswimByFreq.each { |a| @lswimByFreq << a }
    @lswimByFreq.sort!(& @@descending_sort)

    lrunByFreq = lrun.group('activities.user_id').count
    @lrunByFreq = []
    lrunByFreq.each { |a| @lrunByFreq << a }
    @lrunByFreq.sort!(& @@descending_sort)

    lcycleByFreq = lcycle.group('activities.user_id').count
    @lcycleByFreq = []
    lcycleByFreq.each { |a| @lcycleByFreq << a }
    @lcycleByFreq.sort!(& @@descending_sort)

    lgymByFreq = lgym.group('activities.user_id').count
    @lgymByFreq = []
    lgymByFreq.each { |a| @lgymByFreq << a }
    @lgymByFreq.sort!(& @@descending_sort)
  end

  def listAll

    listAllG = Activity.where("activities.user_id = #{current_user.id}").order(date: :desc)

    #POR SEMANA

    @allactivitiesThisWeek = []
    liAll = listAllG
    liAll.each do |activity|
      if (activity.date.strftime("%W").to_i - DateTime.current.strftime("%W").to_i) == 0
        @allactivitiesThisWeek << activity
      end
    end

    @swimactivitiesThisWeek = []
    lwswim = listAllG.where(actype: 0)
    lwswim.each do |activity|
      if (activity.date.strftime("%W").to_i - DateTime.current.strftime("%W").to_i) == 0
        @swimactivitiesThisWeek << activity
      end
    end

    @runactivitiesThisWeek = []
    lwrun =listAllG.where(actype: 1)
    lwrun.each do |activity|
      if (activity.date.strftime("%W").to_i - DateTime.current.strftime("%W").to_i) == 0
        @runactivitiesThisWeek << activity
      end
    end

    @cycleactivitiesThisWeek = []
    lwcycle = listAllG.where(actype: 2)
    lwcycle.each do |activity|
      if (activity.date.strftime("%W").to_i - DateTime.current.strftime("%W").to_i) == 0
        @cycleactivitiesThisWeek << activity
      end
    end

    @gymactivitiesThisWeek = []
    lwgym = listAllG.where(actype: 3)
    lwgym.each do |activity|
      if (activity.date.strftime("%W").to_i - DateTime.current.strftime("%W").to_i) == 0
        @gymactivitiesThisWeek << activity
      end
    end

   #POR MES

    @allactivitiesThisMonth = []
    lmAll = listAllG
    lmAll.each do |activity|
      if (activity.date.month - DateTime.current.month) == 0
        @allactivitiesThisMonth << activity
      end
    end

    @swimactivitiesThisMonth = []
    lmswim = listAllG.where(actype: 0)
    lmswim.each do |activity|
      if (activity.date.month - DateTime.current.month) == 0
        @swimactivitiesThisMonth << activity
      end
    end

    @runactivitiesThisMonth = []
    lmrun = listAllG.where(actype: 1)
    lmrun.each do |activity|
      if (activity.date.month - DateTime.current.month) == 0
        @runactivitiesThisMonth << activity
      end
    end

    @cycleactivitiesThisMonth = []
    lmcycle = listAllG.where(actype: 2)
    lmcycle.each do |activity|
      if (activity.date.month - DateTime.current.month) == 0
        @cycleactivitiesThisMonth << activity
      end
    end

    @gymactivitiesThisMonth = []
    lmgym = listAllG.where(actype: 3)
    lmgym.each do |activity|
      if (activity.date.month - DateTime.current.month) == 0
        @gymactivitiesThisMonth << activity
      end
    end

    #POR ANO

    @allactivitiesThisYear = []
    lyAll = listAllG
    lyAll.each do |activity|
      if (activity.date.year - DateTime.current.year) == 0
        @allactivitiesThisYear << activity
      end
    end

    @swimactivitiesThisYear = []
    lyswim = listAllG.where(actype: 0)
    lyswim.each do |activity|
      if (activity.date.year - DateTime.current.year) == 0
        @swimactivitiesThisYear << activity
      end
    end

    @runactivitiesThisYear = []
    lyrun =listAllG.where(actype: 1)
    lyrun.each do |activity|
      if (activity.date.year - DateTime.current.year) == 0
        @runactivitiesThisYear << activity
      end
    end

    @cycleactivitiesThisYear = []
    lycycle = listAllG.where(actype: 2)
    lycycle.each do |activity|
      if (activity.date.year - DateTime.current.year) == 0
        @cycleactivitiesThisYear << activity
      end
    end

    @gymactivitiesThisYear= []
    lygym =listAllG.where(actype: 3)
    lygym.each do |activity|
      if (activity.date.year - DateTime.current.year) == 0
        @gymactivitiesThisYear << activity
      end
    end
  end

  private

    def set_user
      if params[:user_id].nil?
        @user = current_user
      else
        @user = User.find(params[:user_id])
      end
    end

    def set_activity
      @activity = Activity.find(params[:id])
    end

    def activity_params
      params.require(:activity).permit(:date, :duration, :actype, :description)
    end

    def acattributes
      params.permit(:pools, :speed, :distance, :sets, :gpx_data)
    end
end
