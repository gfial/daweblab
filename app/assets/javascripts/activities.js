// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

var ready = function() {

    if($('#map-container').size() > 0) {
        feedGPXdata();
    }

};

// Because of rails new TurboLink system it ignores the page specific javascript
// So the page is considered ready when TurboLinks has done loading it with page:load
$(document).on('page:load', ready);
$(document).ready(ready);

// Adds functionality to the select element in the create activity form
function activitySelectJS() {

    var actSelect = $('#activity_actype');

    actSelect.on('change', function () {
        var selection = this.selectedIndex;
        if (selection == 0) {
            $('.running-fields').hide();
            $('.cycling-fields').hide();
            $('.gymn-fields').hide();
            $('.swimming-fields').show();
        }
        else if (selection == 1) {
            $('.swimming-fields').hide();
            $('.cycling-fields').hide();
            $('.gymn-fields').hide();
            $('.running-fields').show();
        }

        else if (selection == 2) {
            $('.swimming-fields').hide();
            $('.running-fields').hide();
            $('.gymn-fields').hide();
            $('.cycling-fields').show();
        }

        else {
            $('.swimming-fields').hide();
            $('.running-fields').hide();
            $('.cycling-fields').hide();
            $('.gymn-fields').show();
        }
    });

    actSelect.trigger('change');
}

function validateGPX() {
    var file = $('#gpx-file')[0].files[0];
    if (file) {
        var reader = new FileReader();
        reader.onloadend = function (e) {
            try {
                var points = getPoints(e.target.result);
                if (points.length > 0) {
                    //setValid($gpx, true);
                    $("#activity-gpx-data").val(JSON.stringify(points));
                    return true;
                } else {
                    throw Error;
                }
            } catch (e) {
            }
        };
        reader.readAsBinaryString(file);
    }
}

function getPoints(gpxData) {
    var points = [];
    $(gpxData).find("*[lat][lon]").has("ele").has("time").each(function () {
        var $this = $(this);
        var lat = parseFloat($this.attr("lat"));
        var lon = parseFloat($this.attr("lon"));
        var alt = parseFloat($this.children("ele").html());
        var time = new Date($this.children("time").html());
        // TODO Validate fields
        points.push({latitude: lat, longitude: lon, altitude: alt, time: time});
    });
    return points;
}

function feedGPXdata() {
    var attrs = JSON.parse($('#activity-attrs').val());
    if (attrs) {
        var gpx = JSON.parse(attrs["gpx_data"]);
        if(gpx) {
            drawMap(gpx, '#activity-map', {
                showMarkers: false,
                showMarkerDuration: false,
                showCheckPoints: false
            });
        }
    }
}