

function usersPageTabJS() {

    // Adds functionality to the tabs, it makes tabs... tabs!
    $('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = $(this).attr('href');

        // Show/Hide Tabs
        $('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        $(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
}

var ready = function() {

    usersPageTabJS();
    setFullCalendar();

};


// Because of rails new TurboLink system it ignores the page specific javascript
// So the page is considered ready when TurboLinks has done loading it with page:load
$(document).on('page:load', ready);
$(document).ready(ready);