/**
 * Created by guilherme on 13-11-2014.
 */

var ready = function() {

    if($("#login-box").length == 1) {
        $("#main-container").addClass("login-bg");
    }

    usersPageTabJS();
    navbarJS();
};

// Because of rails new TurboLink system it ignores the page specific javascript
// So the page is considered ready when TurboLinks has done loading it with page:load
$(document).on('page:load', ready);
$(document).ready(ready);

function navbarJS() {

    var subnav = $(".subnav");
    var searchNav = $("#search-nav");
    var createNav = $("#create-nav");

    // Keys pressed on page
    $(document).keydown(function(e) {

        //ESC key
        if(e.keyCode == 27) {
            subnav.hide();
        }
    });

    // Mouse clicks on page
    $(document).mouseup(function (e)
    {
        if (!subnav.is(e.target) // if the target of the click isn't the container...
            && subnav.has(e.target).length === 0 )// ... nor a descendant of the container
        {
            subnav.hide();
        }
    });

    searchNav.find(".subnav").css({"min-width":searchNav.outerWidth()});

    searchNav.on("click", function() {toogleSubnav(searchNav.find(".subnav"))});
    createNav.on("click", function() {toogleSubnav(createNav.find(".subnav"))});

    function toogleSubnav(subnav) {

        if (subnav.is(":visible"))
            subnav.hide();

        else
            subnav.show();
    }
}

function popBoxJS() {

    var pageHeight = $(window).height();
    var boxContainer = $("#popbox-container");
    var box = $(".popbox");

    box.css({"max-height": (pageHeight - 200 + "px")});
    box.find("input[type='submit']").on("click", function () {
        boxContainer.fadeOut();
    });

    $(".box-backdrop").on("click", function() {
        boxContainer.fadeOut();
    })
}

function setFullCalendar() {
    var calendar = $(".calendar");
    if(calendar.length == 1) {
        calendar.fullCalendar({events: '/users/' + calendar.data("user-id") + '/activities.json'});
    }
}