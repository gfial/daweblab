// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

var ready = function() {

    setFullCalendar();
    setDashboardJS();

};


// Because of rails new TurboLink system it ignores the page specific javascript
// So the page is considered ready when TurboLinks has done loading it with page:load
$(document).on('page:load', ready);
$(document).ready(ready);

function setDashboardJS() {

    // Dynamic sizes
    var teamCratio = 0.87475;
    var teamC = $("#own-teams-container");
    var extraCalHeight = $("#dash-calendar-box").innerHeight() - (teamC.innerWidth()/teamCratio);

    teamC.css({"height":(teamC.innerWidth()/teamCratio)+extraCalHeight});

    $(window).on('resize',function() {
        teamC.css({"height":(teamC.innerWidth()/teamCratio)+extraCalHeight});
    });

    var friendsboxes = $("#dash-friends-container").find(".friend-box");

    friendsboxes.on('click', function() {
        friendsboxes.removeClass("selected");
            $(this).addClass("selected");
    });
}