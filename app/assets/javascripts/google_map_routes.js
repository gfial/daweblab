function drawMap(track_data, mapId) {

    function getTrack(style) {
        var bounds = new google.maps.LatLngBounds();
        var points = [];
        var times = [];
        var i;
        for(i = 0; i < track_data.length; i++) {
            var point = track_data[i];
            var lat = point.latitude;
            var lon = point.longitude;
            var p = new google.maps.LatLng(lat, lon);
            points.push(p);
            times.push(point.time);
            bounds.extend(p);
        }

        var beginDate = times[0];
        var endDate = times[times.length - 1];

        beginPos = points[0];
        endPos = points[points.length - 1];

        var polyline = new google.maps.Polyline({
            path: points,
            strokeColor: style.strokeColor,
            strokeOpacity: style.strokeOpacity,
            strokeWeight: style.strokeWeight
        });

        return {
            route: polyline,
            points:points,
            times:times,
            beginDate: beginDate,
            endDate: endDate,
            beginPos: beginPos,
            endPos: endPos,
            bounds:bounds
        };
    }

    function setupTrackOnMap(track, map) {
        map.fitBounds(track.bounds);
        //draws the route on the map
        track.route.setMap(map);
    }

    var track = getTrack({
        strokeColor : "#ef924b",
        strokeOpacity:.7,
        strokeWeight: 4
    });
    var map = new google.maps.Map($(mapId)[0]);

    setupTrackOnMap(track, map);
}
