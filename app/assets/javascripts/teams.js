var ready = function() {

    if($('#new-team-page').length == 1) {
        friendsDropDown();
        $('#button-add').on('click',function(e){
           var selected = $('#friends option:selected');
           selected.remove();

        });
    }
};


//friendslist no html id do drop
/**function loadFriends(){
    $.getJSON('/friendships.json',{filter: $('#filter').val()},function(data){
var $list = $('#friendslist');
$list.html("");
for(d in data) {
$('<li>').text(data[d].name).appendTo($list);
}
});

$(function(){  //keyup after inserting new carater, keydown before
  $('#filter').keyup(loadFriends);
});
}**/

function friendsDropDown(){
    $.getJSON('/friendships.json',function(data){
      for(d in data)
        $('<option>',{value:data[d].id}).text(data[d].name).appendTo('#friends');
    });
}

// Because of rails new TurboLink system it ignores the page specific javascript
// So the page is considered ready when TurboLinks has done loading it with page:load
$(document).on('page:load', ready);
$(document).ready(ready);
