json.array!(@activities) do |activity|
  json.extract! activity, :id, :description
  json.title activity.actype
  json.start activity.date
  if activity.duration.nil?
    json.end Time.now.utc
  else
    json.end activity.date + activity.duration.seconds
  end
  json.url activity_url(activity, format: :html)
end
