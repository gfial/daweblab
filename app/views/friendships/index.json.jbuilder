json.array!(@friends) do |friend|
  json.extract! friend, :name
  json.link user_url(friend, format: :json)
end
