module ApplicationHelper

  def seconds_to_time(seconds)
    if seconds.nil?
      0
    else
      Time.at(seconds).utc.strftime("%H:%M:%S")
      #[seconds / 3600, seconds / 60 % 60, seconds % 60].map { |t| t.to_s.rjust(2,'0') }.join(':')
    end
  end

end
