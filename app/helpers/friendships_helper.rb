module FriendshipsHelper

  def get_friendships(user)
    user.friendships.merge(Friendship.accepted) + user.inverse_friendships.merge(Friendship.accepted)
  end

  def get_friends(user)
    user.friends.merge(Friendship.accepted) + user.inverse_friends.merge(Friendship.accepted)
  end

end
