class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :actype
      t.datetime :date
      t.float :duration
      t.text :description
      t.text :acattributes

      t.belongs_to :user
      t.belongs_to :place

      t.timestamps
    end
  end
end
