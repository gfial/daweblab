# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#PLACES
place1 = Place.new
place1.name = "Parque da Paz"
place1.description = "Great to find peace and silence"
place1.save!

place2 = Place.new
place2.name = "Monsanto"
place2.description = "A well rated place for a diversity of sports"
place2.save!

place3 = Place.new
place3.name = "Ginasio"
place3.description = "Most known gymn around here"
place3.save!

place4 = Place.new
place4.name = "Jardim da Ajuda"
place4.description = "A well rated place for a diversity of sports"
place4.save!

place5 = Place.new
place5.name = "Ocean's 11"
place5.description = "A well rated place for a diversity of sports"
place5.save!

place6 = Place.new
place6.name = "Serra de Sintra"
place6.description = "A well rated place for a diversity of sports, forest and nature all around, its purifing"
place6.save!

place7 = Place.new
place7.name = "Serra da Estrela"
place7.description = "A beautiful view for sure, but be sure to warm yourself up"
place7.save!

place8 = Place.new
place8.name = "Hidden Park"
place8.description = "A well rated place for a diversity of sports"
place8.save!

place9 = Place.new
place9.name = "Ocean's 12"
place9.description = "A well rated place for a diversity of sports"
place9.save!

place10 = Place.new
place10.name = "Parque do Polis"
place10.description = "A well rated place for a diversity of sports"
place10.save!

place11 = Place.new
place11.name = "Squirrel Garden"
place11.description = "So many squirrels! Beware not to step on one"
place11.save!

place12 = Place.new
place12.name = "Gym Sloth"
place12.description = "A different kind of Gym"
place12.save!

place13 = Place.new
place13.name = "Ocean's 13"
place13.description = "A well rated place for a diversity of sports"
place13.save!

place14 = Place.new
place14.name = "Circuito do Jamor"
place14.description = "A well rated place for a diversity of sports"
place14.save!


#USERS
user1 = User.new
user1.email = 'jsloth@example.com'
user1.name = "James Sloyth"
user1.age = 50
user1.sex = "X"
user1.password = 'password'
user1.password_confirmation = 'password'
user1.save!

user2 = User.new
user2.email = 'aslaugh@example.com'
user2.name = "Alex O'Slothin"
user2.age = 26
user2.sex = "X"
user2.password = 'password'
user2.password_confirmation = 'password'
user2.save!

user3 = User.new
user3.email = 'natSlothman@example.com'
user3.name = "Natalie Slothman"
user3.age = 32
user3.sex = "X"
user3.password = 'password'
user3.password_confirmation = 'password'
user3.save!

user4 = User.new
user4.email = 'milas@example.com'
user4.name = "Mila Slothis"
user4.age = 29
user4.sex = "Female"
user4.password = 'password'
user4.password_confirmation = 'password'
user4.save!

user5 = User.new
user5.email = 'eddie@example.com'
user5.name = "Eddie Slothder"
user5.age = 39
user5.sex = "X"
user5.password = 'password'
user5.password_confirmation = 'password'
user5.save!

user6 = User.new
user6.email = 'titi@example.com'
user6.name = "Tiago Slothes"
user6.age = 26
user6.sex = "Male"
user6.password = 'password'
user6.password_confirmation = 'password'
user6.save!

user7 = User.new
user7.email = 'fifi@example.com'
user7.name = "Guilherme Slothial"
user7.age = 21
user7.sex = "Male"
user7.password = 'password'
user7.password_confirmation = 'password'
user7.save!

user8 = User.new
user8.email = 'gui@example.com'
user8.name = "Guilherme Slotho"
user8.age = 21
user8.sex = "Male"
user8.password = 'password'
user8.password_confirmation = 'password'
user8.save!

user9 = User.new
user9.email = 'paris@example.com'
user9.name = "Joana Slothris"
user9.age = 21
user9.sex = "Female"
user9.password = 'password'
user9.password_confirmation = 'password'
user9.save!

user10 = User.new
user10.email = 'nunius@example.com'
user10.name = "Nuno Slothins"
user10.age = 20
user10.sex = "Male"
user10.password = 'password'
user10.password_confirmation = 'password'
user10.save!

user11 = User.new
user11.email = 'soneca@example.com'
user11.name = "Sonecas Slothie"
user11.age = 5
user11.sex = "X"
user11.password = 'password'
user11.password_confirmation = 'password'
user11.save!

user12 = User.new
user12.email = 'rita@example.com'
user12.name = "Rita Slothrge"
user12.age = 23
user12.sex = "Female"
user12.password = 'password'
user12.password_confirmation = 'password'
user12.save!

user13 = User.new
user13.email = 'archer@example.com'
user13.name = "Slothling Archer"
user13.age = 31
user13.sex = "Male"
user13.password = 'password'
user13.password_confirmation = 'password'
user13.save!

user14 = User.new
user14.email = 'kane@example.com'
user14.name = "Slothna Kane"
user14.age = 32
user14.sex = "Female"
user14.password = 'password'
user14.password_confirmation = 'password'
user14.save!

user15 = User.new
user15.email = 'runs@example.com'
user15.name = "Runaway Slothrand"
user15.age = 21
user15.sex = "Male"
user15.password = 'password'
user15.password_confirmation = 'password'
user15.save!


#ACTIVITIES
activity1 = Activity.new
activity1.place = place1
activity1.actype = 0
activity1.duration = 34.0
activity1.description = ""
activity1.acattributes = "{\"pools\": 23}"
activity1.user = user1
activity1.date = DateTime.current
activity1.save!

activity2 = Activity.new
activity2.place = place2
activity2.actype = 1
activity2.duration = 50.0
activity2.description = "Running through the florest with the dogs"
activity2.acattributes = "{\"speed\": 15,\"distance\": 8}"
activity2.user = user2
activity2.date = DateTime.current
activity2.save!

activity3 = Activity.new
activity3.place = place4
activity3.actype = 2
activity3.duration = 60.0
activity3.description = "Just seeing the view with my bike"
activity3.acattributes = "{\"speed\": 20,\"distance\": 3}"
activity3.user = user1
activity3.date = DateTime.current
activity3.save!

activity4 = Activity.new
activity4.place = place3
activity4.actype = 3
activity4.duration = 12.0
activity4.description = "Getting fit, Muscle Lady"
activity4.acattributes = "{\"sets\": 5}"
activity4.user = user3
activity4.date = DateTime.current
activity4.save!

activity5 = Activity.new
activity5.place = place5
activity5.actype = 0
activity5.duration = 60.0
activity5.description = "Finding Nemo"
activity5.acattributes = "{\"pools\": 50}"
activity5.user = user4
activity5.date = DateTime.current
activity5.save!

activity6 = Activity.new
activity6.place = place6
activity6.actype = 2
activity6.duration = 30.0
activity6.description = ""
activity6.acattributes = "{\"speed\": 21,\"distance\": 13}"
activity6.user = user5
activity6.date = DateTime.current
activity6.save!

activity7 = Activity.new
activity7.place = place7
activity7.actype = 1
activity7.duration = 23.0
activity7.description = ""
activity7.acattributes = "{\"speed\": 1,\"distance\": 4}"
activity7.user = user6
activity7.date = DateTime.current
activity7.save!

activity8 = Activity.new
activity8.place = place8
activity8.actype = 2
activity8.duration = 10.0
activity8.description = "Cycle Forrest Cycle"
activity8.acattributes = "{\"speed\": 10,\"distance\": 8}"
activity8.user = user7
activity8.date = DateTime.current
activity8.save!

activity9 = Activity.new
activity9.place = place9
activity9.actype = 3
activity9.duration = 33.0
activity9.description = "Gymn,Gymn, I wanna Gymn with youuu"
activity9.acattributes = "{\"sets\": 1}"
activity9.user = user8
activity9.date = DateTime.current
activity9.save!

activity10 = Activity.new
activity10.place = place13
activity10.actype = 0
activity10.duration = 45.0
activity10.description = ""
activity10.acattributes = "{\"pools\": 35}"
activity10.user = user9
activity10.date = DateTime.current
activity10.save!

activity11 = Activity.new
activity11.place = place11
activity11.actype = 1
activity11.duration = 120
activity11.description = ""
activity11.acattributes = "{\"speed\": 9,\"distance\": 10}"
activity11.user = user10
activity11.date = DateTime.current
activity11.save!

activity12 = Activity.new
activity12.place = place12
activity12.actype = 2
activity12.duration = 34.0
activity12.description = ""
activity12.acattributes = "{\"speed\": 22,\"distance\": 20}"
activity12.user = user11
activity12.date = DateTime.current
activity12.save!

activity13 = Activity.new
activity13.place = place10
activity13.actype = 2
activity13.duration = 40.0
activity13.description = ""
activity13.acattributes = "{\"speed\": 11,\"distance\": 8}"
activity13.user = user12
activity13.date = DateTime.current
activity13.save!

activity14 = Activity.new
activity14.place = place8
activity14.actype = 1
activity14.duration = 55.0
activity14.description = ""
activity14.acattributes = "{\"speed\": 5,\"distance\": 8}"
activity14.user = user13
activity14.date = DateTime.current
activity14.save!

activity15 = Activity.new
activity15.place = place1
activity15.actype = 0
activity15.duration = 30.0
activity15.description = ""
activity15.acattributes =  "{\"pools\": 10}"
activity15.user = user14
activity15.date = DateTime.current
activity15.save!

activity16 = Activity.new
activity16.place = place14
activity16.actype = 2
activity16.duration = 15.0
activity16.description = "I'm cyclinggg, na Reboleira"
activity16.acattributes = "{\"speed\": 17,\"distance\": 7}"
activity16.user = user15
activity16.date = DateTime.current
activity16.save!

activity17 = Activity.new
activity17.place = place12
activity17.actype = 3
activity17.duration = 60.0
activity17.description = "SLOTHINGGGGG!!!"
activity17.acattributes = "{\"sets\": 10}"
activity17.user = user8
activity17.date = DateTime.current
activity17.save!

activity18 = Activity.new
activity18.place = place7
activity18.actype = 1
activity18.duration = 44.0
activity18.description = "Gone with the run"
activity18.acattributes = "{\"speed\": 1,\"distance\": 1}"
activity18.user = user13
activity18.date = DateTime.current
activity18.save!

#FRIENDHIPS
friendships1 = Friendship.new
friendships1.friend_id = user2.id
friendships1.status = "accepted"
friendships1.save!
user1.friendships << friendships1
user1.save!

friendships2 = Friendship.new
friendships2.friend_id = user3.id
friendships2.status = "accepted"
friendships2.save!
user1.friendships << friendships2
user1.save!

friendships3 = Friendship.new
friendships3.friend_id = user2.id
friendships3.status = "accepted"
friendships3.save!
user3.friendships << friendships3
user3.save!

friendships4 = Friendship.new
friendships4.friend_id = user4.id
friendships4.status = "accepted"
friendships4.save!
user1.friendships << friendships4
user1.save!

friendships5 = Friendship.new
friendships5.friend_id = user5.id
friendships5.status = "accepted"
friendships5.save!
user1.friendships << friendships5
user1.save!

friendships6 = Friendship.new
friendships6.friend_id = user6.id
friendships6.status = "accepted"
friendships6.save!
user1.friendships << friendships6
user1.save!

friendships7 = Friendship.new
friendships7.friend_id = user4.id
friendships7.status = "accepted"
friendships7.save!
user3.friendships << friendships7
user3.save!

friendships8 = Friendship.new
friendships8.friend_id = user5.id
friendships8.status = "accepted"
friendships8.save!
user4.friendships << friendships8
user4.save!

friendships9 = Friendship.new
friendships9.friend_id = user7.id
friendships9.status = "accepted"
friendships9.save!
user6.friendships << friendships9
user6.save!

friendships10 = Friendship.new
friendships10.friend_id = user13.id
friendships10.status = "accepted"
friendships10.save!
user8.friendships << friendships10
user8.save!

friendships11 = Friendship.new
friendships11.friend_id = user9.id
friendships11.status = "accepted"
friendships11.save!
user15.friendships << friendships11
user15.save!

friendships12 = Friendship.new
friendships12.friend_id = user11.id
friendships12.status = "accepted"
friendships12.save!
user10.friendships << friendships12
user10.save!

friendships13 = Friendship.new
friendships13.friend_id = user7.id
friendships13.status = "accepted"
friendships13.save!
user9.friendships << friendships13
user9.save!

friendships14 = Friendship.new
friendships14.friend_id = user2.id
friendships14.status = "accepted"
friendships14.save!
user12.friendships << friendships14
user12.save!

friendships15 = Friendship.new
friendships15.friend_id = user4.id
friendships15.status = "accepted"
friendships15.save!
user13.friendships << friendships15
user13.save!

#TEAMS
team1 = Team.new
team1.name = "Slothiamos"
team1.captain = user1
team1.users << [user2,user1]
team1.save!

team2 = Team.new
team2.captain = user2
team2.name = "Slothius"
team2.users << [user1,user3,user2]
team2.save!

team3 = Team.new
team3.captain = user1
team3.name = "Slothim"
team3.users << [user4,user5,user1]
team3.save!

team4 = Team.new
team4.captain = user13
team4.name = "Sloth's Moves"
team4.users << [user8,user4,user13]
team4.save!

team5 = Team.new
team5.captain = user2
team5.name = "We R the Sloths"
team5.users << [user12,user1,user3,user2]
team5.save!

team6 = Team.new
team6.captain = user9
team6.name = "Like the Slothmer of 69"
team6.users << [user9,user15]
team6.save!

team7 = Team.new
team7.captain = user1
team7.name = "ISISloth"
team7.users << [user4,user6,user1]
team7.save!

team8 = Team.new
team8.captain = user6
team8.name = "Game of Sloths"
team8.users << [user1,user7,user6]
team8.save!

team9 = Team.new
team9.captain = user7
team9.name = "Sloths of Castamere"
team9.users << [user9,user6,user7]
team9.save!

team10 = Team.new
team10.captain = user1
team10.name = "Basket Sloth"
team10.users << [user2,user5,user4,user6,user3,user1]
team10.save!